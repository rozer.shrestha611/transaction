package com.nicasia.transaction.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class TransactionProps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String keyData;
    private String valueData;
    @CreatedDate
    private Date createdDate;
    @LastModifiedDate
    private Date lastModified;
    @Version
    private Long version = 1L;
    @ManyToOne
    @JoinColumn(name="txtEntryId")
    private TransactionEntry transactionEntry;
}
