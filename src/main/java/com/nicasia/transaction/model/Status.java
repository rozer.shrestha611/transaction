package com.nicasia.transaction.model;

public enum Status {
    SUCCESS,
    FAILED,
    PENDING,
    AMBIGUOUS
}
