package com.nicasia.transaction.model;

public enum TransactionType {
    DR,
    CR
}
