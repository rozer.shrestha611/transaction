package com.nicasia.transaction.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class TransactionEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    private String initiator;
    private Long mainAmount;
    private String remark;
    @Column(length = 10, unique = true)
    private String uniqueId;
    @DateTimeFormat
    private Date txtInitiatedDate;
    @DateTimeFormat
    private Date txtCompletedDate;
    private Status txtStatus;
    @CreatedDate
    private Date createdDate;
    @LastModifiedDate
    private Date lastModified;
    @Version
    private Long version = 1L;
}
