package com.nicasia.transaction.repository;

import com.nicasia.transaction.model.TransactionEntry;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TransactionRepository extends PagingAndSortingRepository<TransactionEntry, Long> {

    TransactionEntry findByUniqueId(String uniqueId);
}
