package com.nicasia.transaction.controller;

import com.nicasia.transaction.dto.CreateTransactionDto;
import com.nicasia.transaction.dto.GeneralTransactionResponse;
import com.nicasia.transaction.service.TransactionService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="api/transaction/")
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public GeneralTransactionResponse createTransaction(CreateTransactionDto createTransactionDto){
        return transactionService.createTransaction(createTransactionDto);
    }
}
