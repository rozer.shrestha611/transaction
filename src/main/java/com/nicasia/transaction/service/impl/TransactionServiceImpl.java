package com.nicasia.transaction.service.impl;

import com.nicasia.transaction.dto.CreateTransactionDto;
import com.nicasia.transaction.dto.GeneralTransactionResponse;
import com.nicasia.transaction.exception.ClientException;
import com.nicasia.transaction.model.Status;
import com.nicasia.transaction.model.TransactionEntry;
import com.nicasia.transaction.model.TransactionJournal;
import com.nicasia.transaction.repository.TransactionRepository;
import com.nicasia.transaction.service.TransactionService;

import javax.transaction.Transactional;
import java.util.Date;

public class TransactionServiceImpl implements TransactionService {

    private final TransactionService transactionService;
    private final TransactionRepository transactionRepository;

    public TransactionServiceImpl(TransactionService transactionService, TransactionRepository transactionRepository) {
        this.transactionService = transactionService;
        this.transactionRepository = transactionRepository;
    }

    @Override
    @Transactional
    public GeneralTransactionResponse createTransaction(CreateTransactionDto createTransactionDto) {

        TransactionEntry transactionEntryCheck=transactionRepository.findByUniqueId(createTransactionDto.getUniqueId());
        if(transactionEntryCheck!=null){
            throw new ClientException("Unique Id already exist");
        }
        TransactionEntry transactionEntry=new TransactionEntry();
        transactionEntry.setInitiator(createTransactionDto.getInitiator());
        transactionEntry.setMainAmount(createTransactionDto.getMainAmount());
        transactionEntry.setRemark(createTransactionDto.getRemark() );
        transactionEntry.setTxtInitiatedDate(new Date());
        transactionEntry.setTxtCompletedDate(new Date());
        transactionEntry.setTxtStatus(Status.SUCCESS);
        transactionEntry.setCreatedDate(new Date());
        transactionEntry.setLastModified(new Date());
        TransactionEntry savedItem= transactionRepository.save(transactionEntry);

        TransactionJournal transactionJournal=new TransactionJournal();
        tra


        return null;
    }
}
