package com.nicasia.transaction.service;

import com.nicasia.transaction.dto.CreateTransactionDto;
import com.nicasia.transaction.dto.GeneralTransactionResponse;
import org.springframework.stereotype.Service;

@Service
public interface TransactionService {
    GeneralTransactionResponse createTransaction(CreateTransactionDto createTransactionDto);
}
