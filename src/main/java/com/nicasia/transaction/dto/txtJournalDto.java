package com.nicasia.transaction.dto;

import com.nicasia.transaction.model.TransactionType;
import lombok.Data;

@Data
public class txtJournalDto {
    private String fromAc;
    private String toAc;
    private TransactionType transactionType;
    private Long amount;
}
