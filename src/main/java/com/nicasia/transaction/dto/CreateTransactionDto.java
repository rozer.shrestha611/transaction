package com.nicasia.transaction.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class CreateTransactionDto {
    private String initiator;
    private Long mainAmount;
    private String remark;
    private String uniqueId;
//    private Map<String, String> props;
    private List<txtJournalDto> txtJournals;
}
