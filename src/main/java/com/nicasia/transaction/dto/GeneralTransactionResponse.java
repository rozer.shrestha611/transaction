package com.nicasia.transaction.dto;

import lombok.Data;

@Data
public class GeneralTransactionResponse {
    private String message;
    private int code;
}
